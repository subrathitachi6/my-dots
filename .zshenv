export ZDOTDIR="$HOME/.config/zsh"
export CM_LAUNCHER='rofi'
export EDITOR='nvim'
export BROWSER='firefox'
export XDG_DATA_HOME=${XDG_DATA_HOME:="$HOME/.local/share"}
export XDG_CACHE_HOME=${XDG_CACHE_HOME:="$HOME/.cache"}
export XDG_CONFIG_HOME=${XDG_CONFIG_HOME:="$HOME/.config"}
export LESSHISTFILE=-
export GOPATH="$HOME/.config/go"
export CONAN_USER_HOME="$HOME/.config/.conan"
export CARGO_HOME="$HOME/.config/.cargo"
export PASSWORD_STORE_DIR="$HOME/.config/.password-store"
#export GNUPGHOME="$XDG_DATA_HOME"/gnupg
#export SCREENRC="$XDG_CONFIG_HOME"/screen/screenrc
#export GTK2_RC_FILES="$XDG_CONFIG_HOME"/gtk-2.0/gtkrc
#export KDEHOME="$XDG_CONFIG_HOME"/kde
#export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME"/java
export GTK_USE_PORTAL=1

#export QT_QPA_PLATFORMTHEME=qt5ct
PF_INFO="ascii" 
#QT_PLUGIN_PATH=$HOME/.kde4/lib/kde4/plugins/:/usr/lib/kde4/plugins/
